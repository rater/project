# Rater

The current rating and review system is "broken". There are various different kinds of rating systems, the most common being the 5 star and thumbs up & down systems, but they all have fundamental flaws.

The goal of this new _Rater_ project is that to come up with a better design for rating and reviews, both for publishers and consumers, and provide simple reference implementations if possible.



## Problems with the Current Rating Systems

The rating and review systems currently being used are rather "imperfect" to mildly put it. In fact, they all have serious problems which make them more harmful, rather than beneficial, to the whole ecosystem which use the given rating system.

Among numerous problems, we see the following five as the fundamental issues:

* Statistical problem: Whether it's a Facebook page, or an app on an app store, or a product in a product catalog or ecommerce site, the vast majority of the items have no or very few ratings and reviews. For these pages, apps, products, ..., the ratings do not accurately reflect the true nature of the quality of the items. The statistical bias is just too big for this kind of rating system to be useful. The first 1-star rating after a product is released essentially kills the product because that the first rating carries too much (mathematically unsound) weight.
* Manipulation problem: It's related to the first problem. When a product has no or few ratings and reviews, it's extremely easy to manipluate the ratings. There is actually an underground market where you can "buy" ratings, for any type of products. You, as a publisher, can easily shift your overall rating to higher with a few bucks. Furthermore, many websites and platforms like Yelp manipulate ratings for a variety of reasons. (For example, businesses with higher ratings tend to buy more ads, and hence it's in Yelp's best interest to move the overall ratings higher.) Therefore, you, as a consumer, cannot trust these ratings because they simply do not correctly reflect the users overall sentiments, as a group.
* Silent majority problem: One of the unique characteristics of user/consumer ratings is that they do not follow the central limit theorem. In many cases, under very broad and reasonable assumptions, a collection of a large number of values asymptotically reach a Gaussian distribution. But, for ratings, the distributions often have two peaks. This is because the vast majority of the people simply do not participate in the rating and review system. Only people with _strong opinions_ take the time to write a review or even give a numeric rating as simple as 1 through 5. These people are, by defintion, outliers, and yet all current rating systems rely on the ratings from these outliers and use them as if they are true reflection of the majority. (Clearly, there is some positive correlation.) The strong opinions tend to be polarized. Few people will have strong opinions like "I strongly believe this product is just average." Rather, strong opinions tend to be "I think this product is terrible" or "This product is the best product I've tried", etc. This makes the rating distribution to have generally two peaks.
* Time averaging problem: Many things evolve over time. Things change. This is especially so for products and apps and places where the common rating system is often used. A 1-star or 5-star rating receivied three years ago carries, or should carry, a different weight than a 1-star or 5-start rating received today. Many products get better over time, and the ratings could be much better if they were released today, and yet their ratings are much lower than they should have been simply because they have been around longer than their competitors', for instance. The reverse also holds true. A great product a few years ago gets worse over time (e.g., relatively speaking, compared to the competition, etc.) through neglect, etc. Again, the rating system does not reflect the true nature of the quality of the products, restaurants, etc.
* Drive-by-Rating problem: Often the ratings received are not from "true users". People who use your product on a regular basis might be relatively happy with your product, who may or may not feel the particularly strong need or desire to "go back" and rate it. Some of the ratings you receive on your product are often from new users who had bad first impressions, for instance, or from the users who didn't want to take the time to learn how to use your product. Clearly, this reflects certain aspects of the quality of your product. But, the problem is that this often dispropritionately represents the public ratings. Users who used your product for 2 minutes give your product 1 star, whereas many of the regular users of your product (silent majority) may think it was good enough (say, implicitily 3-star in her mind) and did not bother to give your product "the average rating". Many true raters are interested in your response (not necessarily in reply to their reviews). If they had complaints about certain features of your proudcts, or services in your restaurant, they would be interested in the follow-up. They would be interested in whether the problem has been corrected. In some cases, if the problem is corrected, then they revise their rating/review (which makes the overall rating closer to "the true value"). On the other hand, these drive-by-rating users are not true customers and they simply do not reflect the true quality of the products or services.


These problems make the participants in the ecosystem rather unhappy. Publishers/producers/manufacturers are not happy with their unfair ratings and reviews.
User/consumers do not get accurate information they are seeking.

This often hinders the growth a particular ecosystem (often a marketplace of some kind),
and sometimes a less-than-ideal rating system ultimately leads, or contributes, to the demise of an ecosystem.


## Goals of a Rating System

So, what is a rating and review system? What is thier purpose?

Clearly, it serves many purposes, but one of the most important functions of a rating system is that it functions as a recommendation engine. 

It provides certain opinions of the similar users who have tried certain products which you are interested in. For example, _I'm looking to buy a certain product and there are a few similar products in the market. Which one should I buy?_ Etc. In some cases, it also provides social validation. _I tried this product and I liked it, and as it turns that many other peopel who tried this product also liked it._ Etc. _I'm thinking of watching this newly released movie. Is it any good? Should I go watch it or just pass?_ Etc.

Note that all individuals are different. The fact that a friend of mine, or a stranger, liked it does not mean I'll like it too. The rating system can never provide that. (Although companies like Amazon and Netflix do personalized recommendations, but they are really beyond the simple functions of a rating system.) The goal of a rating system is _to quantify the overall sentiment of a group of users toward a specific product._



## So, What?

The goal of the _rater_ project is to invent a better rating and review system
so that it can provide high-fidelity ratings.


In paricular, our goal is to make the system usable even when the statistical averaging is not entirely feasible.


## Call for participation

If you think it's a worthwhile problem to solve, please drop us a line. We can work together to come up with a better solution.


## Additional Info

There are some notes/drafts in the `notes` folder pertinent to the problems we are trying to solve.


