## Statistics

In the last U.S. presidential election,
all major polls failed to correctly predict the outcome of the election. (It was a binary problem, one candidae vs the other.)

Many major political polls use a sample size of 1000 or so people,
and they claim that their margin of error is +-3%. 
This is ridiculous. The U.S. voting population is in the tens of millions. The fact that they claim they can peredict the U.S. prsential election with 1000 sample size is simply beyond laughable. 

I am sure these poll companies hire many Ph.D.s in statistics, etc., and hence their claims often rise to the level of fraud. The U.S. presidential election is an extremely complex problem with many different aspects of social and political issues. People vote for a parituclar candidate for many different reasons. If you have a simple problem like coin-tossing, then the sample size of 1000 can be sufficient (again, depending on the precise probelem). However, not for the U.S. presidential election.

Besides, this ridiculous 3% margin of error number is based on the assumption that you can do perfert random sampling, which is not possible, in practice. 

Statistics is one of the subjects that are so familiar to the general population (who do not necessarily have deep knowledge in statistics) and yet it is one of the subjects that is most misunderstood. One of the common and egregious mistakes peoeple (especially those in media) consistently make is the fact that they interpret correlation as causality. Inferring causality from correlation is a difficult subject, and in most cases correlation does not imply causality. And yet, time and again, you hear this again and again. _Stock market rose because the sugar price went up today._ _It was found that high sodium intake causes general unhappiness in population._ Or, whatever.


Now, back to the subject matter on hand, the rating system.

The vast majority of products and services recieve very few ratings. How on earth do you even presume that those 3 or 5 ratings have any use for the purpose of "rating"? And yet, virtually all websites and platforms which use some kind of rating systems use these numbers as if they serve any useful pruposes.


Why?

Because they don't know better? Or, because they don't have any better alternatives even if they realize how ridiculous their systems are?


I am not sure. But, the fact that these unfair and unscientific rating systems are so pervasive tells somthing. Either they don't care or they serve some useful purposes for their business and that's good enough for them.

However, is it good enough for you?

Suppose that you released an app to an app store. You have 100 downloads, or 100 active monthly users, or whatever. No ratings. And then, one user posts a review with one start, "this is a garbage". Now your app is dead. Nobody, or very few new uses, will ever try your app again because this one rating nad one review carries so much weight out of proportion. Do you think that one start rating and review speaks for all other users who current use, or have tried, your app?

Hell, no.


So, what are you going to do about it?

Let's invent a better rating system, and let you app store know that there are better rating systems than the current inferior system.


Join us, if you are interested, in inventing a better rating system.



