## Statistics

We face two fundamental statistical problems in a rating system.

1. There are, in general, not enough users who are willing to spend time to rate a product or write a review.
2. There are, in general, not enough users who use any given product. Period.


There is not much you can do about the second problem. If nobody comes to your restaurant, then you cannot get a statistically sound rating for your restaurant. There may or may not be a solution (when it comes to a rating system), but we will not concern ourselves with this issue too much. Maybe later. But, not for now.


The first problem is easier to tackle. there is often a "1%" rule of thumb. Typically, one out of 100 users or customers will do reviews and ratings. This is not an exact number, and it will vary from platforms to platforms and from products to products. But, overall this "1%" gives you a good guideline. If you see a product with 10 ratings, you can roughly estimate that the app has, or has had, an order of 1000 users, for example. Again, it's just a rule of thumb. For a particular product, people may have stronger opinions for various reasons, and there may be disportionately many ratings and reviews relative to the actual number of users.



Now, how to solve this problem: the problem of computing a reliable estimate of "true rating" from a small number of ratings which a given product or sevice actually received.



## Bayesian Inference

There is a method of esttimating a value given evidences.
One such method, proven to be "optimal", is bayesian inference. 

First, we will have to assume that there is an inherent "true rating" for each product. We don't know what it is, but there is one. Let's suppose that a given product can have a large number of users, say, a million or possibly a billion, or infinite. And, each and every user gives his or her rating. We can average them or otherwise create other statistic. These are, in some sense, "true values". These are hypothetical values since (a) No product can have an infinite number of users, and (b) Not everybody expresses his or her opinion (via a rating system).

And yet, we can assume that such a value exists. (We can talk about various statistic measures, but for convenience we can only talk about a single number, e.g., an average of the scalar rating values.)



## First Problem


Now, we can formulate the problem this way then.

Given a number of users who use a product, X, and a number of users who rate the product, Y, with rating values, how do you best estimate the "true rating"?

The goal is to show the true rating to the users, potential customers, rather than the actual values received from a few users, which we claim to be "outliers".

As mentioned in README,  there is also a time-averaging problem, etc., but for now, we will focus on a simpler problem. Everyting is for now, at this very moment. You have 1000 users, and 10 out 1000 rate your product right now. What would be the "true rating" of your product?







